FROM php:8.0.8-fpm-alpine

# install PHP extensions -> pdo and pdo_mysql
RUN docker-php-ext-install pdo pdo_mysql mysqli