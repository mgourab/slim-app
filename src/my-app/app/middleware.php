<?php

declare(strict_types=1);

// use App\Application\Middleware\SessionMiddleware;
use Slim\App;
use App\Application\Middleware\ExampleBeforeMiddleware;
use App\Application\Middleware\ExampleAfterMiddleware;


return function (App $app) {
    // Debugging middleware, we need to use the settings from the container we created.
    $settings = $app->getContainer()->get('settings');
    $app->addErrorMiddleware(
      $settings['displayErrorDetails'],
      $settings['logErrors'],
      $settings['logErrorDetails']
    );

    $app->add(ExampleBeforeMiddleware::class);
    $app->add(ExampleAfterMiddleware::class);
};

?>
