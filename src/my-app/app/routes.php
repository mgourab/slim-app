<?php

declare(strict_types=1);

use Slim\App;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface as ResponseInterface;

return function (App $app) {
    $app->get('/hello/{name}', function (RequestInterface $request, ResponseInterface $response, $args) {
      $name = $args['name'];
      $response->getBody()->write("Hello, $name");
      return $response;
    });
};

?>
