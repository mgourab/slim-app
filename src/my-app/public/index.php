<?php

declare(strict_types=1);

use DI\Container;
use Slim\Factory\AppFactory;
// use Psr\Http\Message\RequestInterface;
// use App\Application\Middleware\ExampleBeforeMiddleware;
// use App\Application\Middleware\ExampleAfterMiddleware;
// use Psr\Http\Message\ResponseInterface as ResponseInterface;

require __DIR__ . '/../vendor/autoload.php';

// create a new container object and instantiate it
$container = new Container;

// do the settings connection to our app which has the DI container
$settings = require __DIR__ . '/../app/settings.php';
$settings($container);

// add Logger
$logger = require __DIR__ . '/../app/logger.php';
$logger($container);


// set container on app
AppFactory::setContainer($container);

$app = AppFactory::create();	// static create

// connect index.php file to the middleware
$middleware = require __DIR__ . '/../app/middleware.php';
$middleware($app);


// way to do the global middleware add
// $app->add(ExampleBeforeMiddleware::class);
// $app->add(ExampleAfterMiddleware::class);


// define app routes - add route definitions
// $app->get('/hello/{name}', function (RequestInterface $request, ResponseInterface $response, $args) {
// 	$name = $args['name'];
// 	$response->getBody()->write("Hello, $name");
// 	return $response;
// });
// }); ->add(new ExampleAfterMiddleware())->add(new ExampleBeforeMiddleware());
// this is specific route middleware add and just another way to write the before and after middleware
// instead of doing the global way, we can use this for specific route.

// connect index.php file to the middleware
$routes = require __DIR__ . '/../app/routes.php';
$routes($app);

// Run app
$app->run();
